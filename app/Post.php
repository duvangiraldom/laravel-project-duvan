<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //Table Name
    protected $table = 'posts';
    
    public $privaryKey = 'id';
    public $timestaps = true;
    
    public function user(){
        return $this->belongsTo('App\User');
    }

    //Un Post puede tener muchos comments
    public function comments(){
        return $this->hasMany('App\Comment');
    }
}

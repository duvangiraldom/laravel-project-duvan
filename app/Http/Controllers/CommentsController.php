<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use DB;
Use App\Post;
class CommentsController extends Controller
{
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::orderBy('created_at','desc')->paginate(1);
        return view('comments.index')-> with('comments',$comments);
    }

    public function create()
    {
        return view('comments.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]);

        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->user_id = auth()->user()->id;
        $comment->post_id = $request->input('post_id');
        $comment->save();
        return redirect('/comments')->with('success', 'Comment Created');
    }

    public function show($id)
    {
        $comment = Comment::find($id);
        return view('comments.show')->with('comment', $comment);
    }

    public function edit($id)
    {
        $comment = Comment::find($id);
        return view('comments.edit')->with('comment', $comment);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'body' => 'required'
        ]);

        $comment = Comment::find($id);
        $comment->body = $request->input('body');
        $comment->save();
        return redirect('/comments')->with('success', 'Comment Updated');
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);
        $comment->delete();
        return redirect('/comments')->with('success', 'Comment Removed');
    }
}

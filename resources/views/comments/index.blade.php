@extends('layouts.app')

@section('content')
    <h1>Comments</h1>
    @if(count($comments) > 0)
        @foreach($comments as $comment)
            <div class="well">
            <h3><a href="/comments/{{$comment->id}}">{!!$comment->body!!}</a></h3>
            <h3>User id= {!!$comment->user_id!!}</h3>
            <h3>Post id= {!!$comment->post_id!!}</h3>
                <small>Written on {{$comment->created_at}} by {{$comment->user->name}}</small>
            </div>
        @endforeach
        {{$comments->links()}}
    @else
        <p>No Comments found</p>
    @endif
@endsection
